export interface ICflareWorkerRoute {
  id: string;
  pattern: string;
  script: string;
}
